package capital.any.hazelcast;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

import capital.any.base.enums.DBParameter;
import capital.any.service.AnalyticsInvestmentService;
import capital.any.service.CountryService;
import capital.any.service.CurrencyService;
import capital.any.service.DayCountConventionService;
import capital.any.service.EmailActionService;
import capital.any.service.EmailTemplateService;
import capital.any.service.FileStatusService;
import capital.any.service.IProductService;
import capital.any.service.InvestmentPossibilitiesService;
import capital.any.service.InvestmentStatusService;
import capital.any.service.IssueChannelService;
import capital.any.service.IssueDirectionService;
import capital.any.service.IssueReachedStatusService;
import capital.any.service.IssueReactionService;
import capital.any.service.IssueSubjectService;
import capital.any.service.LanguageService;
import capital.any.service.LocaleService;
import capital.any.service.MarketGroupService;
import capital.any.service.MarketService;
import capital.any.service.MarketingLandingPageService;
import capital.any.service.MessageResourceService;
import capital.any.service.ProductBarrierTypeService;
import capital.any.service.ProductCategoryService;
import capital.any.service.ProductCouponFrequencyService;
import capital.any.service.ProductCouponService;
import capital.any.service.ProductMarketService;
import capital.any.service.ProductMaturityService;
import capital.any.service.ProductStatusService;
import capital.any.service.ProductTypeService;
import capital.any.service.QmQuestionService;
import capital.any.service.RegulationQuestionnaireStatusService;
import capital.any.service.TransactionOperationService;
import capital.any.service.TransactionPaymentTypeService;
import capital.any.service.TransactionStatusService;
import capital.any.service.FileTypeService;
import capital.any.service.WriterService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
@ManagedResource
public class HazelcastService implements CommandLineRunner {
	private static final Logger logger = LoggerFactory.getLogger(HazelcastService.class);
	
	@Autowired
	private HazelcastInstance instance;

	public void addMap(Map<? extends Object,? extends Object> object, String name, boolean evictMap) {
		IMap<Object, Object> map = instance.getMap(name);
		if (evictMap) {
			map.evictAll();
		}
		map.putAll(object);
	}
	
	/**
	 * @param key
	 * @param value
	 * @param mapName
	 * @param ttl - in seconds
	 */
	public void addMapWithTTL(Object key, Object value, String mapName, long ttl, boolean evictMap) {
		IMap<Object, Object> map = instance.getMap(mapName);
		if (evictMap) {
			map.evictAll();
		}
		map.put(key, value, ttl, TimeUnit.SECONDS);
	}
	
	@Autowired
	@Qualifier("CountryServiceHC")
	private CountryService countries;
	@Autowired
	@Qualifier("CurrencyServiceHC")
	private CurrencyService currencies;
	@Autowired
	@Qualifier("LanguageServiceHC")
	private LanguageService languages;
	@Autowired
	@Qualifier("MarketServiceHC")
	private MarketService markets;
	@Autowired
	@Qualifier("MarketGroupServiceHC")
	private MarketGroupService marketGroup; 
	@Autowired
	@Qualifier("ProductServiceHC")
	private IProductService products;
	@Autowired
	@Qualifier("ProductCategoryServiceHC")
	private ProductCategoryService productCategories;
	@Autowired
	@Qualifier("ProductCouponServiceHC")
	private ProductCouponService productCoupons;
	@Autowired
	@Qualifier("ProductMarketServiceHC")
	private ProductMarketService productMarkets;
	@Autowired
	@Qualifier("ProductStatusServiceHC")
	private ProductStatusService productStatuses;
	@Autowired
	@Qualifier("ProductTypeServiceHC")
	private ProductTypeService productTypes;
	@Autowired
	@Qualifier("WriterServiceHC")
	private WriterService writers;
	@Autowired
	@Qualifier("ProductCouponFrequencyServiceHC")
	private ProductCouponFrequencyService productCouponFrequencyService;
	@Autowired
	@Qualifier("DayCountConventionServiceHC")
	private DayCountConventionService dayCountConventionService;
	@Autowired
	@Qualifier("ProductMaturityServiceHC")
	private ProductMaturityService productMaturityService;
	@Autowired
	@Qualifier("ProductBarrierTypeServiceHC")
	private ProductBarrierTypeService productBarrierTypeService;
	@Autowired
	@Qualifier("MessageResourceServiceHC")
	private MessageResourceService messageResourceService;
	@Autowired
	@Qualifier("TransactionStatusServiceHC")
	private TransactionStatusService transactionStatusesService;
	@Autowired
	@Qualifier("TransactionPaymentTypeServiceHC")
	private TransactionPaymentTypeService transactionPaymentTypeService;
	@Autowired
	@Qualifier("TransactionOperationServiceHC")
	private TransactionOperationService transactionOperationService;
	@Autowired
	@Qualifier("InvestmentPossibilitiesServiceHC")
	private InvestmentPossibilitiesService investmentPossibilitiesService;
	@Autowired
	@Qualifier("InvestmentStatusServiceHC")
	private InvestmentStatusService investmentStatusService;
	@Autowired
	@Qualifier("IssueChannelServiceHC")
	private IssueChannelService issueChannelService;
	@Autowired
	@Qualifier("IssueSubjectServiceHC")
	private IssueSubjectService issueSubjectService;
	@Autowired
	@Qualifier("IssueDirectionServiceHC")
	private IssueDirectionService issueDirectionService;
	@Autowired
	@Qualifier("IssueReachedStatusServiceHC")
	private IssueReachedStatusService issueReachedStatusService;
	@Autowired
	@Qualifier("IssueReactionServiceHC")
	private IssueReactionService issueReactionService;
	@Autowired
	@Qualifier("FileTypeServiceHC")
	private FileTypeService fileTypeService;
	@Autowired
	@Qualifier("QmQuestionServiceHC")
	private QmQuestionService qmQuestionService;
	@Autowired
	@Qualifier("RegulationQuestionnaireStatusServiceHC")
	private RegulationQuestionnaireStatusService regulationQuestionnaireStatusService;
	@Autowired
	@Qualifier("MarketingLandingPageServiceHC")
	private MarketingLandingPageService marketingLandingPageService;
	@Autowired
	@Qualifier("EmailTemplateServiceHC")
	private EmailTemplateService emailTemplateService;
	@Autowired
	@Qualifier("DBParameterServiceHC")
	private capital.any.service.DBParameterService DBParameterService;
	@Autowired
	@Qualifier("AnalyticsInvestmentServiceHC")
	private AnalyticsInvestmentService analyticsInvestmentService;
	@Autowired
	@Qualifier("AllowIPServiceHC")
	private capital.any.service.AllowIPService allowIPService;
	@Autowired
	@Qualifier("EmailActionServiceHC")
	private EmailActionService emailActionService;
	@Autowired
	@Qualifier("LocaleServiceHC")
	private LocaleService localeService;
	@Autowired
	@Qualifier("FileStatusServiceHC")
	private FileStatusService fileStatusService;
	
	@Override
	public void run(String... arg0) throws Exception {
		loadMaps(false);
	}
	
	@ManagedOperation
	public void reloadMaps() throws Exception {
		logger.info("Reload all info in HC - Start.");
		loadMaps(true);
		logger.info("Reload all info in HC - End.");
	}
	
	private void loadMaps(boolean evictMap) {
		addMap(this.countries.get(), Constants.MAP_COUNTRIES, evictMap);
		addMap(this.currencies.get(), Constants.MAP_CURRENCIES, evictMap);
		addMap(this.languages.get(), Constants.MAP_LANGUAGES, evictMap);
		//we load only markets that have price!
		addMap(this.markets.getMarketsWithLastPrice(), Constants.MAP_MARKETS, evictMap); 
		addMap(this.markets.getThin(), Constants.MAP_MARKETS_THIN, evictMap);
		addMap(this.marketGroup.get(), Constants.MAP_MARKET_GROUPS, evictMap);
		addMap(this.products.getProducts(), Constants.MAP_PRODUCTS, evictMap);
		addMap(this.productCategories.get(), Constants.MAP_PRODUCT_CATEGORIES, evictMap);
		//addMap(this.productCoupons.get(), Constants.MAP_PRODUCT_COUPONS);
		//addMap(this.productMarkets.get(), Constants.MAP_PRODUCT_MARKETS);
		addMap(this.productStatuses.get(), Constants.MAP_PRODUCT_STATUSES, evictMap);
		addMap(this.productTypes.get(), Constants.MAP_PRODUCT_TYPES, evictMap);
		addMap(this.writers.get(), Constants.MAP_WRITERS, evictMap);
		addMap(this.productCouponFrequencyService.get(), Constants.MAP_PRODUCT_COUPON_FREQUENCIES, evictMap);
		addMap(this.dayCountConventionService.get(), Constants.MAP_DAY_COUNT_CONVENTION, evictMap);
		addMap(this.productMaturityService.get(), Constants.MAP_PRODUCT_MATURITY, evictMap);
		addMap(this.productBarrierTypeService.get(), Constants.MAP_PRODUCT_BARRIER_TYPE, evictMap);
		addMap(this.messageResourceService.get(), Constants.MAP_MESSAGE_RESOURCE, evictMap);
		addMap(this.transactionStatusesService.get(), Constants.MAP_TRANSACTION_STATUSES, evictMap);
		addMap(this.transactionPaymentTypeService.get(), Constants.MAP_TRANSACTION_PAYMENT_TYPES, evictMap);
		addMap(this.transactionOperationService.get(), Constants.MAP_TRANSACTION_OPERATION, evictMap);
		addMap(this.investmentPossibilitiesService.get(), Constants.MAP_INVESTMENT_POSSIBILITIES, evictMap);
		addMap(this.investmentStatusService.get(), Constants.MAP_INVESTMENT_STATUSES, evictMap);
		addMap(this.issueChannelService.get(), Constants.MAP_ISSUE_CHANNELS, evictMap);
		addMap(this.issueSubjectService.get(), Constants.MAP_ISSUE_SUBJECTS, evictMap);
		addMap(this.issueDirectionService.get(), Constants.MAP_ISSUE_DIRECTIONS, evictMap);
		addMap(this.issueReachedStatusService.get(), Constants.MAP_REACHED_STATUSES, evictMap);
		addMap(this.issueReactionService.get(), Constants.MAP_ISSUE_REACTIONS, evictMap);
		addMap(this.fileTypeService.get(), Constants.MAP_FILE_TYPE, evictMap);
		addMap(this.qmQuestionService.getAll(), Constants.MAP_REGULATION_QUESTIONNAIRE, evictMap);
		addMap(this.regulationQuestionnaireStatusService.getScoreStatus(), Constants.MAP_SCORE_STATUS, evictMap);
		addMap(this.marketingLandingPageService.getAll(), Constants.MAP_MARKETING_LANDING_PAGE, evictMap);
		addMap(this.emailTemplateService.get(), Constants.MAP_EMAIL_TEMPLATE, evictMap);
		addMap(this.DBParameterService.get(), Constants.MAP_DB_PARAMETER, evictMap);
		addMap(this.markets.getUnderlyingAssets(), Constants.MAP_MARKET_UNDERLYING_ASSETS, evictMap);
		addMap(this.allowIPService.get(), Constants.MAP_ALLOW_IP, evictMap);
		addMap(this.emailActionService.get(), Constants.MAP_EMAIL_ACTION, evictMap);
		addMap(this.localeService.get(), Constants.MAP_LOCALE, evictMap);
		addMap(this.fileStatusService.get(), Constants.MAP_FILE_STATUS, evictMap);
		
		addMapWithTTL(0, this.analyticsInvestmentService.getAnalyticsInvestment(), Constants.MAP_ANALYTICS_INVESTMENT, DBParameterService.get().get(DBParameter.HC_TTL_ANALYTICS.getId()).getNumValue(), evictMap);
	}
}