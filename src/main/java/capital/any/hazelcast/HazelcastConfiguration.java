package capital.any.hazelcast;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;


/**
 * @author LioR SoLoMoN
 *
 */
public class HazelcastConfiguration {
	private static final int INFINITE = 0; 
	@Value("${hazelcast.maincenter.url}")
	private String SERVICE_URL;
	
	@Bean
	public Config config() {
		Config config = new Config();
		config.addMapConfig( 
			  	new MapConfig()
			    .setName("MAPS")
			    //.setEvictionPolicy(EvictionPolicy.LRU)
			    .setTimeToLiveSeconds(INFINITE))
			  	.setProperty("hazelcast.logging.type","slf4j");
		config.getNetworkConfig().getJoin().getMulticastConfig().setEnabled(false);
		config.getManagementCenterConfig().setEnabled(true)
        		.setUrl(SERVICE_URL)
        		.setUpdateInterval(3);
		return config;
	}
}
