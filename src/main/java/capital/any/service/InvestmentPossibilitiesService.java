package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.investmentPossibilities.IInvestmentPossibilitiesDao;
import capital.any.model.base.InvestmentPossibility;
import capital.any.service.base.investmentPossibilities.IInvestmentPossibilitiesService;

/**
 * @author Eyal G
 *
 */
@Service("InvestmentPossibilitiesServiceHC")
public class InvestmentPossibilitiesService implements IInvestmentPossibilitiesService {
	
	@Autowired
	private IInvestmentPossibilitiesDao investmentPossibilitiesDao;

	@Override
	public Map<Integer, InvestmentPossibility> get() {
		return investmentPossibilitiesDao.get();
	}

	@Override
	@Deprecated
	public InvestmentPossibility get(int currencyId) {
		return null;
	}
}
