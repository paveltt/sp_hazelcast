package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.issueSubject.IIssueSubjectDao;
import capital.any.model.base.IssueSubject;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service("IssueSubjectServiceHC")
public class IssueSubjectService {
	@Autowired
	private IIssueSubjectDao issueSubjectDao;
	
	public Map<Integer, IssueSubject> get() {
		return issueSubjectDao.get();
	}
}
