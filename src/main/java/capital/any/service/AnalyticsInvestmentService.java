package capital.any.service;

import org.springframework.stereotype.Service;

import capital.any.model.base.analytics.AnalyticsInvestment;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service("AnalyticsInvestmentServiceHC")
public class AnalyticsInvestmentService extends capital.any.service.base.analyticsInvestment.AnalyticsInvestmentService {
	
	public AnalyticsInvestment getAnalyticsInvestment() {
		AnalyticsInvestment analyticsInvestment = get();
        if (analyticsInvestment == null) {
              analyticsInvestment = new AnalyticsInvestment();
        }
        analyticsInvestment.setListAnalyticsInvestmentProduct(getByProduct());
        return analyticsInvestment;
	}
	
}
