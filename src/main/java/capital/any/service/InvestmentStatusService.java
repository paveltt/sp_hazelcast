package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.investmentStatus.IInvestmentStatusDao;
import capital.any.model.base.InvestmentStatus;

/**
 * @author Eyal Goren
 *
 */
@Service("InvestmentStatusServiceHC")
public class InvestmentStatusService {
	@Autowired
	private IInvestmentStatusDao investmentStatusDao;

	public Map<Integer, InvestmentStatus> get() {
		return investmentStatusDao.get();
	}
}
