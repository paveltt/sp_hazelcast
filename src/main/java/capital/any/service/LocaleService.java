package capital.any.service;

import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.service.base.locale.ILocaleService;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service("LocaleServiceHC")
public class LocaleService {
	
	@Autowired
	private ILocaleService localeService;
	
	public Map<String, Locale> get() {
		return localeService.get();
	}
}
