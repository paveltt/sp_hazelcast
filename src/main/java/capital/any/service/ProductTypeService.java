package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productType.IProductTypeDao;
import capital.any.model.base.ProductType;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("ProductTypeServiceHC")
public class ProductTypeService {
	@Autowired
	private IProductTypeDao productTypeDao;

	public Map<Integer, ProductType> get() {	
		return productTypeDao.get();
	}
}
