package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.issueChannel.IIssueChannelDao;
import capital.any.model.base.IssueChannel;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service("IssueChannelServiceHC")
public class IssueChannelService {
	@Autowired
	private IIssueChannelDao issueChannelDao;
	
	public Map<Integer, IssueChannel> get() {
		return issueChannelDao.get();
	}
}
