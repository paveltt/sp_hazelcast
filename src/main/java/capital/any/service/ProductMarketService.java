package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productMarket.IProductMarketDao;
import capital.any.model.base.ProductMarket;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("ProductMarketServiceHC")
public class ProductMarketService {
	@Autowired
	private IProductMarketDao productMarketDao;

	public Map<Long, ProductMarket> get() {
		return productMarketDao.get();
	}
}
