package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.marketGroup.IMarketGroupDao;
import capital.any.model.base.MarketGroup;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("MarketGroupServiceHC")
public class MarketGroupService {
	@Autowired
	private IMarketGroupDao marketGroupDao;

	public Map<Integer, MarketGroup> get() {
		return marketGroupDao.get(); 
	}
}
