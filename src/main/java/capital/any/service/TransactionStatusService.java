package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.transactionStatus.ITransactionStatusDao;
import capital.any.model.base.TransactionStatus;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("TransactionStatusServiceHC")
public class TransactionStatusService {
	@Autowired
	private ITransactionStatusDao transactionStatusDao;

	public Map<Integer, TransactionStatus> get() {
		return transactionStatusDao.get();
	}
}
