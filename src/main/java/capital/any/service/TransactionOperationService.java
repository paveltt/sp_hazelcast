package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.transactionOperation.ITransactionOperationDao;
import capital.any.model.base.TransactionOperation;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("TransactionOperationServiceHC")
public class TransactionOperationService {
	@Autowired
	private ITransactionOperationDao transactionOperationDao;

	public Map<Integer, TransactionOperation> get() {
		return transactionOperationDao.get();
	}
}
