package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.issueReachedStatus.IIssueReachedStatusDao;
import capital.any.model.base.IssueReachedStatus;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service("IssueReachedStatusServiceHC")
public class IssueReachedStatusService {
	@Autowired
	private IIssueReachedStatusDao issueReachedStatusDao;
	
	public Map<Integer, IssueReachedStatus> get() {
		return issueReachedStatusDao.get();
	}
}
