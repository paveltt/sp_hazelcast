package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.writer.IWriterDao;
import capital.any.model.base.Writer;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("WriterServiceHC")
public class WriterService {
	@Autowired
	private IWriterDao writerDao;

	public Map<Integer, Writer> get() {
		return writerDao.get();
	}
}
