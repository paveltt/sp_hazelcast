package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.emailTemplate.IEmailTemplateDao;
import capital.any.model.base.EmailTemplate;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service("EmailTemplateServiceHC")
public class EmailTemplateService {
	@Autowired
	private IEmailTemplateDao emailTemplateDao;
	
	public Map<Integer, EmailTemplate> get() {
		return emailTemplateDao.get();
	}
}
