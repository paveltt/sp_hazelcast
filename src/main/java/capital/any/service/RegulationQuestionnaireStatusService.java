package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.regulationQuestionnaireStatus.IRegulationQuestionnaireStatusDao;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service("RegulationQuestionnaireStatusServiceHC")
public class RegulationQuestionnaireStatusService {
	@Autowired
	private IRegulationQuestionnaireStatusDao regulationQuestionnaireStatusDao;
	
	public Map<Long, Integer> getScoreStatus() {
		return regulationQuestionnaireStatusDao.getScoreStatus();
	}
}
