package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.base.enums.MsgResTypeEnum;
import capital.any.dao.base.messageResource.IMessageResourceDao;
import capital.any.model.base.MsgRes;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("MessageResourceServiceHC")
public class MessageResourceService {
	@Autowired
	private IMessageResourceDao messageResourceDao;

	public Map<Integer, Map<Integer, Map<String, String>>> get() {
		return messageResourceDao.get();
	}
}
