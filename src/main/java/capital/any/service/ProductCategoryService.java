package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productCategory.IProductCategoryDao;
import capital.any.model.base.ProductCategory;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("ProductCategoryServiceHC")
public class ProductCategoryService {
	@Autowired
	private IProductCategoryDao productCategoryDao;
	
	public Map<Integer, ProductCategory> get() {		
		return productCategoryDao.get();
	}
}
