package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productMaturity.IProductMaturityDao;
import capital.any.model.base.ProductMaturity;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("ProductMaturityServiceHC")
public class ProductMaturityService {
	@Autowired
	private IProductMaturityDao productMaturityDao;

	public Map<Integer, ProductMaturity> get() {
		return productMaturityDao.get();
	}
}
