package capital.any.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productCoupon.IProductCouponDao;
import capital.any.model.base.ProductCoupon;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("ProductCouponServiceHC")
public class ProductCouponService {
	@Autowired
	private IProductCouponDao productCouponDao;
	
	public Map<Long, List<ProductCoupon>> get() {	
		return productCouponDao.get(); 
	}
}
