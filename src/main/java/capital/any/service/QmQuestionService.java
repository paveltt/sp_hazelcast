package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.qmQuestion.IQmQuestionDao;
import capital.any.model.base.QmQuestion;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service("QmQuestionServiceHC")
public class QmQuestionService {
	@Autowired
	private IQmQuestionDao qmQuestionDao;
	
	public Map<Integer, QmQuestion> getAll() {
		return qmQuestionDao.getAll();
	}
}
