package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.product.IProductDao;
import capital.any.model.base.Product;
import capital.any.service.IProductService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("ProductServiceHC")
public class ProductService extends capital.any.service.base.product.ProductService implements IProductService {
	@Autowired
	private IProductDao productDao;	
	
	public Map<Long, Product> getProducts() {
		 Map<Long, Product> products = productDao.getProducts();
		setProductsFull(products.values());
		return products;
	}
}
