package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.dbParameter.IDBParameterDao;
import capital.any.model.base.DBParameter;

/**
 * @author Eyal G
 *
 */
@Service("DBParameterServiceHC")
public class DBParameterService {
	
	@Autowired
	private IDBParameterDao dbParameterDao;	
	
	public Map<Integer, DBParameter> get() {
		return dbParameterDao.get();
	}
}
