package capital.any.service;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.fileType.IFileTypeDao;
import capital.any.model.base.FileType;

/**
 * @author Eyal G
 *
 */
@Service("FileTypeServiceHC")
public class FileTypeService {
	private static final Logger logger = LoggerFactory.getLogger(FileTypeService.class);
	@Autowired
	private IFileTypeDao fileTypeDao;
	
	public Map<Integer, FileType> get() {
		return fileTypeDao.get(); 
	}	
}
