package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.dayCountConvention.IDayCountConventionDao;
import capital.any.model.base.DayCountConvention;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("DayCountConventionServiceHC")
public class DayCountConventionService {	
	@Autowired
	private IDayCountConventionDao dayCountConventionDao;

	public Map<Integer, DayCountConvention> get() {
		return dayCountConventionDao.get();
	}
}
