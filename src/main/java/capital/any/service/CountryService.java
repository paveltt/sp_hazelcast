package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.country.ICountryDao;
import capital.any.model.base.Country;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("CountryServiceHC")
public class CountryService {
	@Autowired
	private ICountryDao countryDao;

	public Map<Integer, Country> get() {
		return countryDao.get();
	}
}
