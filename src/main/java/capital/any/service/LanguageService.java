package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.language.ILanguageDao;
import capital.any.model.base.Language;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("LanguageServiceHC")
public class LanguageService {	
	@Autowired
	private ILanguageDao languageDao;

	public Map<Integer, Language> get() {
		return languageDao.get();
	}
}
