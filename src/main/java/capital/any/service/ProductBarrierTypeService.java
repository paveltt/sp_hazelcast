package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productBarrierType.IProductBarrierTypeDao;
import capital.any.model.base.ProductBarrierType;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("ProductBarrierTypeServiceHC")
public class ProductBarrierTypeService {	
	@Autowired
	protected IProductBarrierTypeDao productBarrierTypeDao;

	public Map<Integer, ProductBarrierType> get() {
		return productBarrierTypeDao.get();
	}	
}
