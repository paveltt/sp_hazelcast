package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.marketingLandingPage.IMarketingLandingPageDao;
import capital.any.model.base.MarketingLandingPage;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service("MarketingLandingPageServiceHC")
public class MarketingLandingPageService {
	@Autowired
	private IMarketingLandingPageDao marketingLandingPageDao;
	
	public Map<Integer, MarketingLandingPage> getAll() {
		return marketingLandingPageDao.getAll();
	}
}
