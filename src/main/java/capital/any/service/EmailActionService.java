package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.emailAction.IEmailActionDao;
import capital.any.model.base.EmailAction;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service("EmailActionServiceHC")
public class EmailActionService {
	
	@Autowired
	private IEmailActionDao emailActionDao;
	
	public Map<Integer, EmailAction> get() {
		Map<Integer, EmailAction> emailActions = emailActionDao.get();
		return emailActions;
	}

}
