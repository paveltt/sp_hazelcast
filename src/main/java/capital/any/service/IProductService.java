package capital.any.service;

import java.util.Map;
import capital.any.model.base.Product;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IProductService extends capital.any.service.base.product.IProductService {	
	
	Map<Long, Product> getProducts();
}
