package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.issueDirection.IIssueDirectionDao;
import capital.any.model.base.IssueDirection;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service("IssueDirectionServiceHC")
public class IssueDirectionService {
	@Autowired
	private IIssueDirectionDao issueDirectionDao;
	
	public Map<Integer, IssueDirection> get() {
		return issueDirectionDao.get();
	}
}
