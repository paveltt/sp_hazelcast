package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.transactionPaymentTypes.ITransactionPaymentTypeDao;
import capital.any.model.base.TransactionPaymentType;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("TransactionPaymentTypeServiceHC")
public class TransactionPaymentTypeService {
	@Autowired
	private ITransactionPaymentTypeDao transactionPaymentTypeDao;

	public Map<Integer, TransactionPaymentType> get() {
		return transactionPaymentTypeDao.get();
	}
}
