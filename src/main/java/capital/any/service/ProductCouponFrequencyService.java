package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productCouponFrequency.IProductCouponFrequencyDao;
import capital.any.model.base.ProductCouponFrequency;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("ProductCouponFrequencyServiceHC")
public class ProductCouponFrequencyService {
	@Autowired
	private IProductCouponFrequencyDao productCouponFrequencyDao;

	public Map<Integer, ProductCouponFrequency> get() {
		return productCouponFrequencyDao.get();
	}
}
