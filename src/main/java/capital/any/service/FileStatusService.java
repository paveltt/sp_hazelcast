package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.fileStatus.IFileStatusDao;
import capital.any.model.base.FileStatus;

/**
 * 
 * @author Eyal.o
 *
 */
@Service("FileStatusServiceHC")
public class FileStatusService {
	
	@Autowired
	private IFileStatusDao fileStatusDao;
	
	public Map<Integer, FileStatus> get() {
		Map<Integer, FileStatus> fileStatuses = fileStatusDao.get();
		return fileStatuses;
	}

}
