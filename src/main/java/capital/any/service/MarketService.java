package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.market.IMarketDao;
import capital.any.model.base.Market;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("MarketServiceHC")
public class MarketService {	
	@Autowired
	private IMarketDao marketDao;

	public Map<Integer, Market> getThin() {
		return marketDao.getThin();
	}

	public Map<Integer, Market> getMarketsWithLastPrice() {
		return marketDao.getMarketsWithLastPrice();
	}
	
	public Map<Integer, Market> getUnderlyingAssets() {
		return marketDao.getUnderlyingAssets();
	}
	
}
