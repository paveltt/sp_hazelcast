package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.issueReaction.IIssueReactionDao;
import capital.any.model.base.IssueReaction;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service("IssueReactionServiceHC")
public class IssueReactionService {
	@Autowired
	private IIssueReactionDao issueReactionDao;
	
	public Map<Integer, IssueReaction> get() {
		return issueReactionDao.get();
	}
}
