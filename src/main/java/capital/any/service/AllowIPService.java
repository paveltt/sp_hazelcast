package capital.any.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.allowIP.IAllowIPDao;
import capital.any.model.base.AllowIP;


/**
 * @author eran.levy
 *
 */
@Service("AllowIPServiceHC")
public class AllowIPService {
	
	@Autowired
	private IAllowIPDao allowIPDao;	
	
	public Map<String, AllowIP> get() {
		return allowIPDao.get();
	}
}
