package capital.any.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productStatus.IProductStatusDao;
import capital.any.model.base.ProductStatus;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("ProductStatusServiceHC")
public class ProductStatusService {
	@Autowired
	private IProductStatusDao productStatusDao;

	public Map<Integer, ProductStatus> get() {
		return productStatusDao.get();
	}
}
