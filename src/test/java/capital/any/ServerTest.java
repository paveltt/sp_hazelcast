package capital.any;

import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.core.HazelcastInstance;
import capital.any.hazelcast.Constants;

/**
 * @author LioR SoLoMoN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
public class ServerTest {
	private static final Logger logger = LoggerFactory.getLogger(ServerTest.class);
	HazelcastInstance client;
	
	@Before
	public void init(){
		client = HazelcastClient.newHazelcastClient();
	}

	@Test
	public void countries() {
        Map<Object,Object> countries = client.getMap(Constants.MAP_COUNTRIES);
        Assert.assertTrue(countries.size() > 0);
        logger.info(Constants.MAP_COUNTRIES + " " + countries.size());
	}
	
	@Test
	public void currencies() {
        Map<Object,Object> currencies = client.getMap(Constants.MAP_CURRENCIES);
        Assert.assertTrue(currencies.size() > 0);
        logger.info(Constants.MAP_CURRENCIES + " " + currencies.size());
	}
	
	@Test
	public void languages() {
        Map<Object,Object> languages = client.getMap(Constants.MAP_LANGUAGES);
        Assert.assertTrue(languages.size() > 0);
        logger.info(Constants.MAP_LANGUAGES + " " + languages.size());
	}
	
	@Test
	public void markets() {
		Map<Object,Object> markets = client.getMap(Constants.MAP_MARKETS);
        Assert.assertTrue(markets.size() > 0);
        logger.info(Constants.MAP_MARKETS + " " + markets.size());
	}
	
	@Test
	public void marketsThin() {
		Map<Object,Object> markets = client.getMap(Constants.MAP_MARKETS_THIN);
        Assert.assertTrue(markets.size() > 0);
        logger.info(Constants.MAP_MARKETS_THIN + " " + markets.size());
	}
	
	@Test
	public void marketGroups() {
        Map<Object,Object> marketGroups = client.getMap(Constants.MAP_MARKET_GROUPS);
        Assert.assertTrue(marketGroups.size() > 0);
        logger.info(Constants.MAP_MARKET_GROUPS + " " + marketGroups.size());
	}
	
	@Test
	@Ignore
	public void products() {
        Map<Object,Object> products = client.getMap(Constants.MAP_PRODUCTS);
        Assert.assertTrue(products.size() > 0);
        logger.info(Constants.MAP_PRODUCTS + " " + products.size());
	}
	
	@Test
	public void productCategories() {
        Map<Object,Object> productCategories = client.getMap(Constants.MAP_PRODUCT_CATEGORIES);
        Assert.assertTrue(productCategories.size() > 0);
        logger.info(Constants.MAP_PRODUCT_CATEGORIES + " " + productCategories.size());
	}
	
	@Test
	public void productCoupons() {
        Map<Object,Object> productCoupons = client.getMap(Constants.MAP_PRODUCT_COUPONS);
        Assert.assertTrue(productCoupons.size() > 0);
        logger.info(Constants.MAP_PRODUCT_COUPONS + " " + productCoupons.size());
	}
	
	@Test
	public void productMarkets() {
        Map<Object,Object> productMarkets = client.getMap(Constants.MAP_PRODUCT_MARKETS);
        Assert.assertTrue(productMarkets.size() > 0);
        logger.info(Constants.MAP_PRODUCT_MARKETS + " " + productMarkets.size());
	}
	
	@Test
	public void productStatuses() {
        Map<Object,Object> productStatuses = client.getMap(Constants.MAP_PRODUCT_STATUSES);
        Assert.assertTrue(productStatuses.size() > 0);
        logger.info(Constants.MAP_PRODUCT_STATUSES + " " + productStatuses.size());
	}
	
	@Test
	public void productTypes() {
        Map<Object,Object> productTypes = client.getMap(Constants.MAP_PRODUCT_TYPES);
        Assert.assertTrue(productTypes.size() > 0);
        logger.info(Constants.MAP_PRODUCT_TYPES + " " + productTypes.size());
	}
	
	@Test
	public void writers() {
        Map<Object,Object> writers = client.getMap(Constants.MAP_WRITERS);
        Assert.assertTrue(writers.size() > 0);
        logger.info(Constants.MAP_WRITERS + " " + writers.size());
	}
	
	@Test
	public void issueChannels() {
		Map<Object, Object> issueChannels = client.getMap(Constants.MAP_ISSUE_CHANNELS);
		Assert.assertTrue(issueChannels.size() > 0);
		logger.info(Constants.MAP_ISSUE_CHANNELS + " " + issueChannels.size());
	}
	
	@Test
	public void issueSubjects() {
		Map<Object, Object> issueSubjects = client.getMap(Constants.MAP_ISSUE_SUBJECTS);
		Assert.assertTrue(issueSubjects.size() > 0);
		logger.info(Constants.MAP_ISSUE_SUBJECTS + " " + issueSubjects.size());
	}
	
	@Test
	public void issueDirections() {
		Map<Object, Object> issueDirections = client.getMap(Constants.MAP_ISSUE_DIRECTIONS);
		Assert.assertTrue(issueDirections.size() > 0);
		logger.info(Constants.MAP_ISSUE_DIRECTIONS + " " + issueDirections.size());
	}
	
	@Test
	public void issueReachedStatuses() {
		Map<Object, Object> issueReachedStatuses = client.getMap(Constants.MAP_REACHED_STATUSES);
		Assert.assertTrue(issueReachedStatuses.size() > 0);
		logger.info(Constants.MAP_REACHED_STATUSES + " " + issueReachedStatuses.size());
	}
	
	@Test
	public void issueReactions() {
		Map<Object, Object> issueReactions = client.getMap(Constants.MAP_ISSUE_REACTIONS);
		Assert.assertTrue(issueReactions.size() > 0);
		logger.info(Constants.MAP_ISSUE_REACTIONS + " " + issueReactions.size());
	}
	
	@Test
	public void fileTypes() {
		Map<Object, Object> fileTypes = client.getMap(Constants.MAP_FILE_TYPE);
		Assert.assertTrue(fileTypes.size() > 0);
		logger.info(Constants.MAP_FILE_TYPE + " " + fileTypes.size());
	}
	
	@Test
	public void regulationQuestionnaire() {
		Map<Object, Object> regulationQuestionnaire = client.getMap(Constants.MAP_REGULATION_QUESTIONNAIRE);
		Assert.assertTrue(regulationQuestionnaire.size() > 0);
		logger.info(Constants.MAP_REGULATION_QUESTIONNAIRE + " " + regulationQuestionnaire.size());
	}
	
	@Test
	public void scoreStatus() {
		Map<Object, Object> scoreStatus = client.getMap(Constants.MAP_SCORE_STATUS);
		Assert.assertTrue(scoreStatus.size() > 0);
		logger.info(Constants.MAP_SCORE_STATUS + " " + scoreStatus.size());
	}
	
	@Test
	public void marketingLandingPage() {
		Map<Object, Object> marketingLandingPage = client.getMap(Constants.MAP_MARKETING_LANDING_PAGE);
		Assert.assertTrue(marketingLandingPage.size() > 0);
		logger.info(Constants.MAP_MARKETING_LANDING_PAGE + " " + marketingLandingPage.size());
	}
	
	@Test
	public void emailTemplate() {
		Map<Object, Object> map = client.getMap(Constants.MAP_EMAIL_TEMPLATE);
		Assert.assertTrue(map.size() > 0);
		logger.info(Constants.MAP_EMAIL_TEMPLATE + " " + map.size());
	}
	
	@Test
	public void DBParameter() {
		Map<Object, Object> map = client.getMap(Constants.MAP_DB_PARAMETER);
		Assert.assertTrue(map.size() > 0);
		logger.info(Constants.MAP_DB_PARAMETER + " " + map.size());
	}
	
	@Test
	public void analyticsInvestment() {
		Map<Object, Object> map = client.getMap(Constants.MAP_ANALYTICS_INVESTMENT);
		Assert.assertTrue(map.size() > 0);
		logger.info(Constants.MAP_ANALYTICS_INVESTMENT + " " + map.size());
	}
	
	@Test
	public void allowIP() {
		Map<Object, Object> map = client.getMap(Constants.MAP_ALLOW_IP);
		Assert.assertTrue(map.size() > 0);
		logger.info(Constants.MAP_ALLOW_IP + " " + map.size());
	}
	
	@Test
	public void underlyingAssets() {
		Map<Object, Object> marketUnderlyingAssets = client.getMap(Constants.MAP_MARKET_UNDERLYING_ASSETS);
		Assert.assertTrue(marketUnderlyingAssets.size() > 0);
		logger.info(Constants.MAP_MARKET_UNDERLYING_ASSETS + " " + marketUnderlyingAssets.size());
	}
	
}
